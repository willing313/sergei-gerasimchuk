package com.company;
import java.util.Scanner;
class Akker {
    int fAkkerman(int m, int n) {

        if (m == 0) {
            return n + 1;
        } else {
            if (n > 0) {
                return fAkkerman(m - 1, fAkkerman(m, n - 1));
            } else {
                return fAkkerman(m - 1, 1);
            }
        }

    }
}
public class Ten50 {
    public static void main(String[] args) {
Scanner num=new Scanner(System.in);
Akker k=new Akker();
System.out.print("m= ");
int m1=num.nextInt();
System.out.print("n= ");
int n1=num.nextInt();
System.out.print("с этими значениями m и n функция Аккермана равна "+k.fAkkerman(m1,n1));
    }
}
