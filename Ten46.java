package com.company;
import java.util.Scanner;
class Progressia{
    double prog(double a, double q, int v){
    	double result;
        if (v==1) return a;
        result=prog(a*q,q,v-1);
        return result;
    }
    double sum(double a, double q, double v){
    	double result;
    	if (v==1) return a;
    	result=a*Math.pow(q,v-1)+sum(a,q,v-1);
    	return result;
	}
}
public class Ten46 {

    public static void main(String[] args) {
	Progressia p=new Progressia();
	Scanner n1=new Scanner(System.in);
	System.out.print("первый член геометрической прогрессии= ");
	int a1=n1.nextInt();
	System.out.print("знаменатель геометрической прогрессии= ");
	int q1=n1.nextInt();
	System.out.print("n-ый член геометрической прогрессии= ");
	int v1=n1.nextInt();
	System.out.println(v1+"-ый член прогрессии равен "+p.prog(a1,q1,v1));
	System.out.println("Сумма "+v1+" членов прогрессии равно "+p.sum(a1,q1,v1));
    }
}
