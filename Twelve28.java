package com.company;
import java.util.Scanner;
public class Twelve28 {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        System.out.print("n= ");
        int n = num.nextInt();
        int[][] a = new int[n][n];
        int k = 1;
        for (int m=0;m<=Math.floor(n/2);m++) {
            for (int j = m; j <= n - 2 - m; j++) {
                a[m][j] = k;
                a[m][j + 1] = k + 1;
                k = a[m][j + 1];
            }
            for (int i = m; i <= n - 2 - m; i++) {
                a[i][n - m - 1] = k;
                a[i + 1][n - m - 1] = k + 1;
                k = a[i + 1][n - m - 1];
            }

            for (int j = n - 1 - m; j >= 1 + m; j--) {
                a[n - m - 1][j] = k;
                a[n - m - 1][j - 1] = k + 1;
                k = a[n - m - 1][j - 1];
            }
            for (int i = n - 1 - m; i >= m + 2; i--) {
                a[i][m] = k;
                a[i - 1][m] = k + 1;
                k = a[i - 1][m];
            }
            k=k+1;
        }
        a[(int)Math.floor(n/2)][(int)Math.floor(n/2)]=(int)Math.pow(n,2);  //я не знаю, на сколько это приемлемо
        for (int i = 0; i <= n - 1; i++) {                                 //но хотелось бы верить, что выкрутился :)
            for (int j = 0; j <= n - 1; j++) {                             //(циклы выше не рассчитывают центр змейки)
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}