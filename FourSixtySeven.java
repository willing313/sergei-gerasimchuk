package com.company;

import java.util.Scanner;

public class FourSixtySeven {

    public static void main(String[] args) {
        Scanner num=new Scanner(System.in);
        System.out.print("Введите день года ");
        int n=num.nextInt();
        if (n>365) System.out.print("в году 365 дней, не больше ");
        else { if ((n%7==6)||(n%7==0)) System.out.print("Выходной");
        else System.out.print("рабочий день");
        }
    }
}
