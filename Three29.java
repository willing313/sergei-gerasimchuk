package com.company;
import java.util.Scanner;
public class Three29 {

    public static void main(String[] args) {
	Scanner num=new Scanner(System.in);
	System.out.print("x= " );
	int x=num.nextInt();
	System.out.print("y= " );
	int y=num.nextInt();
	System.out.print("z= " );
	int z=num.nextInt();
/**
 * каждое из чисел X и Y нечетное
  */
if ((x%2==1)||(y%2==1))	System.out.println("true");
else System.out.print("false");

/**
 * только одно из чисел X и Y больше 20
 */
if ((x<20)^(y<20)) System.out.println("true");
else System.out.println("false");

/**
 *хотя бы одно из чисел X или Y равно нулю
 */
if ((x==0)||(y==0)) System.out.println("true");
else System.out.println("false");

/**
 * каждое из чисел X, Y, Z отрицательное;
 */
if ((x<0)&(y<0)&(z<0)) System.out.println("true");
else System.out.println("false");

/**
 * только одно из чисел X, Y и Z кратно пяти
 */
if ((((x%5==0)^(y%5==0))^(z%5==0))&&!((x%5==0)&(y%5==0)&(z%5==0))) System.out.println("true");
else System.out.println("false");
/**
 * хотя бы одно из чисел больше ста
 */
	if ((x>100)|(y>100)|(z>100)) System.out.print("true");
	else System.out.print("false");
    }
}
