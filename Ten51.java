package com.company;
import java.util.Scanner;
class Mth {
    int mth1(int n) {
        if (n > 0) return n;
        return mth1(n - 1);
    }

    int mth2(int n) {
        if (n > 0) return mth2(n - 1);
        return n;
    }

    int mth3(int n) {
        if (n > 0) {
            mth3(n - 1);
            return n;
        }
        else return n;
    }
}
public class Ten51 {

    public static void main(String[] args) {
	Scanner num=new Scanner(System.in);
	Mth k=new Mth();
	System.out.print("n= ");
	int n1=num.nextInt();
	System.out.println("По первой процедуре результат "+k.mth1(n1));
        System.out.println("По второй процедуре результат "+k.mth2(n1));
        System.out.print("По третьей процедуре результат "+k.mth3(n1));
    }
}
