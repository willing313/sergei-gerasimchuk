package com.company;
import java.util.Scanner;

public class Nine17 {

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String str1 = str.next();
        if (str1.charAt(0) == str1.charAt(str1.length()-1)) {
            System.out.println("Первый и последний символы совпадают");
        } else System.out.println("Первый и последние символы не совпадают");
    }
}