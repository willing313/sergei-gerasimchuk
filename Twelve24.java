package com.company;
import java.util.Scanner;
public class Twelve24 {

    public static void main(String[] args) {
        Scanner num=new Scanner(System.in);
        System.out.print("n= ");
        int n=num.nextInt();
        int [][]a=new int[n][n];
        int [][]b=new int[n][n];
        for (int i=0;i<=n-1;i++) {
            for (int j = 0; j <= n-1; j++) {
                if (i == 0 || j == 0) a[i][j] = 1;
                else a[i][j] = a[i][j-1]+a[i-1][j];
            }
        }
        for (int i=0;i<=n-1;i++){
            for (int j=0;j<=n-1;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println("" +
                    "");
        }
        System.out.println(" ");
        for (int i=0;i<=n-1;i++) {
            for (int j = 0; j <= n-1; j++) {
                b[i][j]=(1+i+j)-j*(int)Math.floor((i+j)/(n))-Math.abs(n-j)*(int)Math.floor((i+j)/(n));
            }
        }

        for (int i=0;i<=n-1;i++){
            for (int j=0;j<=n-1;j++){
                System.out.print(b[i][j]+" ");
            }
            System.out.println("" +
                    "");
        }
    }
}
