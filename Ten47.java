package com.company;
import java.util.Scanner;
class fibonacci{
    int fib(int a){
        int result;
        if (a==0) return 0;
        if (a==1||a==2) return 1;
        result=fib(a-1)+fib(a-2);
        return result;
    }
}
public class Ten47 {

    public static void main(String[] args) {
	fibonacci f=new fibonacci();
	Scanner num=new Scanner(System.in);
	System.out.print("k-ый элемент числа Фибоначчи= ");
	int n=num.nextInt();
	System.out.print("этот элемент равен "+f.fib(n));
    }
}
