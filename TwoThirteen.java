package com.company;

import java.util.Scanner;

public class TwoThirteen {

    public static void main(String[] args) {
        Scanner num=new Scanner(System.in);
        System.out.print("Введите трехзначное число от 100 до 200:  ");
        double n=num.nextInt();
        if ((100<n)&&(n<200)) {
            double n1 = (n % 10) * 100 + Math.floor(n / 100) + (Math.floor(n / 10) - Math.floor(n / 100) * 10) * 10;
            System.out.print(n1);
        }   else System.out.print("число не принадлежит промежутку (100;200)");
    }
}
