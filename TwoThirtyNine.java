package com.company;

import java.util.Scanner;

public class TwoThirtyNine {

    public static void main(String[] args) {
        Scanner num=new Scanner(System.in);
        System.out.print("h= ");
        int h=num.nextInt();
        System.out.print("m= ");
        int m=num.nextInt();
        System.out.print("s= ");
        int s=num.nextInt();
        double grad=h*30+m*0.5+s*(0.5/60);
        if (grad>360) grad=grad-360;
        if (grad>180) grad=Math.abs(grad-360);
        System.out.print("угол между часовой стрелкой и началом времени составляет "+grad+" градусов");
    }
}
