package com.company;
import java.util.Scanner;
public class Twelve23 {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        System.out.print("n= ");
        int n = num.nextInt();
        int[][] a = new int[n][n];
        int[][] b = new int[n][n];
        int[][] c = new int[n][n];
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                if ((i == j) || (i + j + 1 == n))
                    a[i][j] = 1;
                else a[i][j] = 0;
            }

        }
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println("" +
                    "");
        }
        System.out.println("" +
                "");
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                if ((i == j) || (i + j + 1 == n) || (i == Math.ceil(n / 2) || (j == Math.ceil(n / 2))))
                    b[i][j] = 1;
                else b[i][j] = 0;
            }

        }
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println("" +
                    "");
        }
        System.out.println("" +
                "");
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                if (((j<=Math.ceil(n/2)))&&((i-j)>0)&&(i<=Math.ceil(n/2))) c[i][j]=0;
                else if (((j<=Math.ceil(n/2)))&&((i+j)<n-1)&&(i>Math.ceil(n/2))) c[i][j]=0;
                else if (((j>Math.ceil(n/2)))&&((i+j)>n-1)&&(i<=Math.ceil(n/2))) c[i][j]=0;
                else if (((j>Math.ceil(n/2)))&&((i-j)<0)&&(i>Math.ceil(n/2))) c[i][j]=0;
                else c[i][j]=1;
            }


        }
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                System.out.print(c[i][j] + " ");
            }
            System.out.println("" +
                    "");

            }
        }

    }
