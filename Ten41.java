package com.company;
import java.util.Scanner;
class Factorial {
    int fact1(int n) {
        int result;

        if (n == 1)
            return 1;
       result=fact1(n-1)*n;
        return result;
    }
}
public class Ten41 {

    public static void main(String[] args) {
	Factorial f=new Factorial();
	Scanner n1=new Scanner(System.in);
	System.out.print("n= ");
	int i=n1.nextInt();
	System.out.println("Факториал числа "+i+" равен "+(f.fact1(i)));
    }
}
