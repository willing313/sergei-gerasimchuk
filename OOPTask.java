package com.company;

import java.util.ArrayList;

interface Hello { //здесь все понятно
    void hello();
    void hello(String name);
}
abstract class People { // абстрактынй класс, который описывает поведение объекта

    public void describeExpierence(String expierence) { //поведение объекта с разным опытом (выпускник или самоучка)
        if (expierence.equals("graduate"))
            System.out.println("I passed successfully getJavaJob exams and code reviews.");
        else if (expierence.equals("self-leaner"))
            System.out.println("I have been learning java by myself and nobody examined how thorough is my knowledge and how good is my code");
    }
}


class Candidate extends People implements Hello { //наследуемся от People и реализуем интерфейс Hello (это "множественное" наследие)
    private String name; //инкапуляция подразумевает использования один из 4-х (public, private, protected, internal) модификаторов доступа
    private String experience; //в данном случае нужен private для скрытности, доступ осуществляется через поля конструктора
    public Candidate(String name, String expierence) { //конструктор для создания кандидата с именем и опытом
        this.name = name;
        this.experience = expierence;
    }
    public void hello(String name) { //переопределяем метод интерфейса
        System.out.println("Hi, my name is " + name);
    }
    public void hello() {//переопределяем метод, в данной случае он нам не нужен
    }
    public String getName (){ // возвращаем имя кандидата
        return this.name;
    }
    public String getExperience (){ //возвращает опыт кандидата
        return this.experience;
    }
}
class Employer implements Hello{ //реализуем интерфейс Hello и ниже переопределяем методы интерфейса
    public void hello() {
        System.out.println("Hi, introduce yourself and describe your java experience ");
    }
    public void hello(String name) {
    }
}

public class OOPTask {

    public static void main(String[] args) {
        ArrayList<Candidate> candidates = new ArrayList<>(); //создаем коллекцию с типом Candidate для хранения всех кандидатов

        //здесь и далее полиморфизм (создаем и добавляем в список 10 кандидатов с разными именем и опытом)
        candidates.add(new Candidate("Petya", "self-leaner"));
        candidates.add(new Candidate("Sergey", "graduate"));
        candidates.add(new Candidate("Ksenia", "graduate"));
        candidates.add(new Candidate("Valentin", "self-leaner"));
        candidates.add(new Candidate("Stanislav", "graduate"));
        candidates.add(new Candidate("Nikolai", "graduate"));
        candidates.add(new Candidate("Georgii", "self-leaner"));
        candidates.add(new Candidate("Vasilii", "self-leaner"));
        candidates.add(new Candidate("Vanya", "graduate"));
        candidates.add(new Candidate("Vitalii", "self-leaner"));

        Employer employer = new Employer(); //здесь тоже полиморфизм, когда создаем объект Employer(работодатель у нас один)

        for (Candidate candidate : candidates){ //foreach
            employer.hello();
            candidate.hello(candidate.getName()); // вызываем метод hello интерфейса и передаем ему имя кандидата
            candidate.describeExpierence(candidate.getExperience()); //вызываем метод describeExperience и передаем опыт кандидата
            System.out.println("------------------------------------------------------------------------------------------------------------");
        }
    }
}



